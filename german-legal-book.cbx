\ProvidesFile{german-legal-book.cbx}[2023/02/22 v003]
%%% license: LPPL 1.3c or later
%%% author-maintainer: Dominik Brodowski, dominik.brodowski@uni-saarland.de


%%%%%%%%%%%%%%%%
%%% Vorspann %%%
%%%%%%%%%%%%%%%%

%%% Dieser Zitierstil basiert auf ext-authortitle, verwendet aber kein
%%% ibidem (Geschmacksfrage!)
\RequireCitationStyle{ext-authortitle}%

%%% Um die Änderungen so klein wie möglich zu halten, wird das Paket
%%% xpatch verwendet, mit dem sich existierende Makros verändern lassen,
%%% ohne sie gänzlich neu definieren zu müssen.
\RequirePackage{xpatch}%

%%% Mit der Option "edsuper" wird angegeben, ob die Auflagennummer bei
%%% Büchern und Kommentaren mit abgedruckt werden soll. Der Standard ist
%%% "true", d.h. um dies zu unterbinden, muss "edsuper=false" als Option
%%% gesetzt werden.
\newbool{book:edition:super}
\setboolean{book:edition:super}{true}
\DeclareBibliographyOption[boolean]{edsuper}[true]{%
  \setboolean{book:edition:super}{#1}%
}

%%% Für eine Zitierung nach Randnummern sei ein neuer String namens
%%% 'marginnumber' definiert.
\NewBibliographyString{marginnumber}
\DefineBibliographyStrings{english}{%
	marginnumber	= {mn.}
}
\DefineBibliographyStrings{german}{%
	marginnumber	= {Rn.}
}


%%%%%%%%%%%%%%%%%%%%%%%%
%%% Allgemeiner Teil %%%
%%%%%%%%%%%%%%%%%%%%%%%%

%%% Mehrere Autorennamen werden durch einen Schrägstrich getrennt. Dieser
%%% ist -- anders als die Autorennamen (siehe bbx) -- recte zu setzen.
\AtEveryCite{%
  \renewcommand*{\multinamedelim}{{\normalfont{\addslash}}}%
  \renewcommand*{\finalnamedelim}{{\normalfont{\addslash}}}%
}

%%% cite:title wird normalerweise verwendet, um alles ab dem Beitragstitel
%%% (d.h. nach dem Autorennamen) auszugeben. Mit dieser "Weiche" kann
%%% hierfür, abhängig vom Beitragstyp, ein eigenes Makro definiert werden.
\renewbibmacro*{cite:title}{%
  \printtext[bibhyperref]{%
    \ifbibmacroundef{cite:\strfield{entrytype}}%
    {%%% default, from authortitle.cbx %%%
      \printfield[citetitle]{labeltitle}%
    }{%%% use custom override %%%
      \usebibmacro*{cite:\strfield{entrytype}}%
    }%
  }%
}

%%% Mit diesem Makro wird standardmäßig die Startseite ausgegeben, wenn
%%% es einer genauen Fundstelle ("pinpoint citation") als postnote fehlt.
\newbibmacro*{startingpage}{%
  \iffieldundef{postnote}%
    {\iffieldundef{pages}{}%
      {\addcomma\addspace\printfield{pages}}}%
    {}%
}

%%% Die Formatdefintion "edition:super" und das Makro "printsuperedition"
%%% werden genutzt, um -- falls book:edition:super auf "true" gesetzt ist
%%% (Option "edsuper", siehe oben) die Auflagennummer (!) hochgestellt
%%% auszugeben.
\DeclareFieldFormat{edition:super}{%
  \ifinteger{#1}{%
    \mkbibsuperscript{\tiny{#1}}%
  }{}%
}

\newbibmacro*{printsuperedition}{%
  \ifbool{book:edition:super}{\printfield[edition:super]{edition}}{}%
}

%%% Einige Felder sollen standardmäßig bei \fullcite{} nicht abgedruckt werden;
%%% zudem soll nur der Nachname (und ggf. Initialen) ausgegeben werden, nicht auch
%%% die Vornamen.
\AtEveryCitekey{%
  \clearlist{publisher}%
  \clearfield{note}%
  \clearfield{series}%
  \clearfield{number}%
  \clearfield{doi}%
  \clearfield{isbn}%
  \clearfield{url}%
  % 
  \DeclareNameAlias{default}{labelname}%
}


%%%%%%%%%%%%%%%
%%% article %%%
%%%%%%%%%%%%%%%

%%% In Fußnoten soll nicht die Seitenspanne des Aufsatzes, sondern nur die
%%% Startseite ausgegeben werden.
\AtEveryCite{%
  \DeclareFieldFormat[article]{pages}{\mkfirstpage{#1}}%
}

%%% Gibt es eine genaue Fundstelle ("pinpoint citation") als sog. postnote,
%%% so soll zunächst die Startseite und danach die genaue Fundstelle in
%%% einer Klammer ausgegeben werden.
\DeclareFieldFormat[article]{postnote}{\addspace\printfield{pages}\addspace\mkbibparens{#1}}

%%% Nachfolgend ist der Fußnoten-Zitierstil für Aufsätze definiert, die
%%% in Zeitschriften erschienen sind, welche nach Erscheinungsjahr (und
%%% nicht Band, dann @periodical) zitiert werden:
\newbibmacro*{cite:article}{%
  %%%
  %%% Zeitschriftenname...
  \usebibmacro{journal}%
  %%%
  %%% Leerzeichen...
  \setunit*{\addspace}%
  %%%
  %%% ggf. Angabe der Ausgabe (wenn Feld "issue" definiert ist)
  \iffieldundef{issue}{}{\printfield{issue}\slash}%
  %%%
  %%% Jahr...
  \printfield{year}%
  %%%
  %%% und dann die Startseite bzw. pinpoint citation
  \usebibmacro{startingpage}%
}


%%%%%%%%%%%%%%%%%%
%%% periodical %%%
%%%%%%%%%%%%%%%%%%

%%% In Fußnoten soll nicht die Seitenspanne des Aufsatzes, sondern nur die
%%% Startseite ausgegeben werden.
\AtEveryCite{%
  \DeclareFieldFormat[periodical]{pages}{\mkfirstpage{#1}}%
}

%%% Gibt es eine genaue Fundstelle ("pinpoint citation") als sog. postnote,
%%% so soll zunächst die Startseite und danach die genaue Fundstelle in
%%% einer Klammer ausgegeben werden.
\DeclareFieldFormat[periodical]{postnote}{\printfield{pages} \mkbibparens{#1}}

%%% Nachfolgend ist der Fußnoten-Zitierstil für Aufsätze definiert, die
%%% in Zeitschriften erschienen sind, welche nach Band (und nicht nach
%%% Erscheinungsjahr, dann @article) zitiert werden:
\newbibmacro*{cite:periodical}{%
  %%%
  %%% Zeitschriftenname...
  \usebibmacro{journal}%
  %%%
  %%% Leerzeichen...
  \setunit*{\addspace}%
  %%%
  %%% Band...
  \printfield{volume}%
  %%%
  %%% Leerzeichen...
  \addspace%
  %%%
  %%% Jahr (in Klammern)...
  \mkbibparens{\printfield{year}}%
  %%%
  %%% Komma...
  \addcomma%
  %%%
  %%% und dann die Startseite bzw. pinpoint citation
  \usebibmacro{startingpage}%
}


%%%%%%%%%%%%
%%% book %%%
%%%%%%%%%%%%

%%% Nachfolgend ist der Fußnoten-Zitierstil für Bücher definiert:
\newbibmacro*{cite:book}{%
  \printfield[citetitle]{labeltitle}%
  \usebibmacro{printsuperedition}%
}


%%%%%%%%%%%%%%
%%% inbook %%%
%%%%%%%%%%%%%%

%%% In Fußnoten soll nicht die Seitenspanne des Aufsatzes, sondern nur die
%%% Startseite (mit S. vorneweg) ausgegeben werden.
\AtEveryCite{%
  \DeclareFieldFormat[inbook]{pages}{%
     \iffieldequalstr{pagination}{section}{%
     	%%% if we cite by margin number, just have the section printed here...
     	\mkfirstpage{#1}%
     }{%
     	%%% otherwise, print the page prefix and the first page here...
     	\bibstring{page}~\mkfirstpage{#1}%
     }%
  }%
}

%%% Gibt es eine genaue Fundstelle ("pinpoint citation") als sog. postnote,
%%% so soll zunächst die Startseite und danach die genaue Fundstelle in
%%% einer Klammer ausgegeben werden. Ausnahme: Ist als pagination 'section'
%%% angegeben, so erfolgt eine Ausgabe nach Randnummern.
\DeclareFieldFormat[inbook]{postnote}{%
   \printfield{pages}%
   \addspace%
   \iffieldequalstr{pagination}{section}{\bibstring{marginnumber}~#1}{\mkbibparens{#1}}%
}

%%% Herausgebernamen in der Fußnote dürfen abgekürzt werden
\AtEveryCite{%
   \DeclareNameAlias[inbook]{byeditor}{labelname}%
}

%%% Nachfolgend ist der Fußnoten-Zitierstil für Buchbeiträge definiert:
\newbibmacro*{cite:inbook}{%
  %%%
  %%% in:
  \usebibmacro{in:}%
  \midsentence% nach dem Doppelpunkt soll ein "von" usw. klein gesetzt werden.
  %%%
  %%% Hrsg. ...
  \usebibmacro{byeditor+others}%
  %%%
  %%% Leerzeichen...
  \newunit\newblock%
  %%%
  %%% dann der Buchtitel...
  \iffieldundef{shorttitle}%
     {\printfield{booktitle}}%
     {\printfield{shorttitle}}%
  \usebibmacro{printsuperedition}%
  %%%
  %%% Leerzeichen...
  \newunit\newblock%
  %%%
  %%% und dann die Startseite bzw. pinpoint citation
  \usebibmacro{startingpage}%
}


%%%%%%%%%%%%%%%%%%%%
%%% incollection %%%
%%%%%%%%%%%%%%%%%%%%

%%% In Fußnoten soll nicht die Seitenspanne des Aufsatzes, sondern nur die
%%% Startseite (mit S. vorneweg) ausgegeben werden.
\AtEveryCite{%
  \DeclareFieldFormat[incollection]{pages}{%
     \bibstring{page}~\mkfirstpage{#1}%
  }%
}

%%% Gibt es eine genaue Fundstelle ("pinpoint citation") als sog. postnote,
%%% so soll zunächst die Startseite und danach die genaue Fundstelle in
%%% einer Klammer ausgegeben werden.
\DeclareFieldFormat[incollection]{postnote}{\printfield{pages} \mkbibparens{#1}}

%%% Nachfolgend ist der Fußnoten-Zitierstil für Festschriftenbeiträge definiert:
\newbibmacro*{cite:incollection}{%
  %%%
  %%% in:
  \usebibmacro{in:}%
  %%%
  %%% Festschriftentitel...
  \iffieldundef{shorttitle}%
     {\printfield{booktitle}}%
     {\printfield{shorttitle}}%
  %%%
  %%% Leerzeichen...
  \newunit\newblock%
  %%%
  %%% und dann die Startseite bzw. pinpoint citation
  \usebibmacro{startingpage}%
}


%%%%%%%%%%%%%%%%%%
%%% commentary %%%
%%%%%%%%%%%%%%%%%%

%%% Der Bearbeiter wird im prenote-Feld angegeben und kursiv gesetzt, danach
%%% folgt ein Komma
\DeclareFieldFormat[commentary]{prenote}{\mkbibemph{#1}\addcomma}%

%%% Der Bearbeiter kann dabei entweder im Klartext angegeben werden, der
%%% dann unverändert ausgegeben wird, oder mit dem Befehl \commentator{citekey}:
%%% Dann werden die Autorennamen aus der Bibliographie entnommen und
%%% entsprechend formatiert.
\DeclareCiteCommand{\commentator}
  {}
  {\mkbibemph{\printnames[labelname]{author}}}
  {}
  {}

%%% Falls bei einem Kommentar "shorthand" angegeben ist, werden die
%%% Herausgeber nicht (!) genannt. Hierzu wird der Befehl
%%% \printnames{labelname} im allgemeinen Makro "textcite" unter die
%%% Bedingung gestellt, dass es sich um einen Kommentar handelt und ein
%%% "shorthand" definiert ist.
\xpatchbibmacro{textcite}{%
  \printnames{labelname}%
}{%
  \ifentrytype{commentary}{%
    \iffieldundef{shorthand}{%
      \printnames{labelname}%
    }{}%
  }{%
    \printnames{labelname}%
  }%
}{}{}

%%% Falls bei einem Kommentar "shorthand" angegeben ist, muss trotzdem das
%%% "in:" vorangestellt und die Auflagennummer nachgeschoben werden. Hierzu
%%% wird das allgemeine Makro "cite:shorthand" entsprechend modifiziert.
\xpatchbibmacro{cite:shorthand}{%
  \printfield{shorthand}%
}{%
  \usebibmacro{in:}%
  \printfield{shorthand}%
  \usebibmacro{printsuperedition}%
}{}{}

%%% Nachfolgend ist der Fußnoten-Zitierstil für Kommentare definiert. Dieser
%%% wird indes nur verwendet, falls "shorthand" nicht definiert ist;
%%% andernfalls erledigen die oben modifizierten Makros "textcite" und
%%% "cite:shorthand" die ganze Arbeit.
\newbibmacro*{cite:commentary}{%
  \usebibmacro{in:}%
  \iffieldundef{shorttitle}%
     {\printfield{booktitle}}%
     {\printfield{shorttitle}}%
}

\endinput
